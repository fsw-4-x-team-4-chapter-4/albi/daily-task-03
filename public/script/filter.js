const filter = function (data, attribute) {
  if (attribute == undefined || attribute == "") {
    return data;
  }
  // let result = []
  let result = data.filter((data) => {
    return data.company.toLowerCase() === attribute;
  });
  return result;
};

module.exports = filter;
