function JsonData4(value) {
  const data4 = value.filter(
    (data) => (data.company === "Pelangi" || data.company === "Intel") &&
    data.eyeColor === "green"
  );
  return data4;
}

const data = require('./data')
module.exports = JsonData4(data);